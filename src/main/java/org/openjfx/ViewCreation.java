package org.openjfx;

import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ViewCreation
{
    Label nom;
    Label categorie;
    Label desc_label;
    FileAdministrator coffee_file_administrator;

    public ViewCreation()
    {
        coffee_file_administrator = new FileAdministrator();
    }


    public Stage screenCreation(Stage primary_stage)
    {
        ArrayList<Cafe> listeCafe= new ArrayList<>();
        

        //Création de la boite de stockage principal
        HBox principal_box = new HBox();
        
        VBox coffee_label = CoffeeLabelCreation();
        
    
        VBox labelBox = new VBox();
        labelBox.setPrefWidth(200);    
        
        String lang = coffee_file_administrator.language_setting();

        try
        {
            listeCafe = coffee_file_administrator.lectureCafe(listeCafe,lang);    
        } catch (Exception e)
        {
            e.printStackTrace();    
        }
        
        labelBox = CoffeelabelBoxInitialisation(labelBox,listeCafe);

        

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(labelBox);

        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        principal_box.getChildren().add(scrollPane);
        principal_box.getChildren().add(coffee_label);

        Scene scene = new Scene(principal_box, 400, 400);
        primary_stage.setScene(scene);

        return primary_stage;
    }


    private VBox CoffeeLabelCreation()
    {
        VBox coffee_label = new VBox();

        coffee_label.setPrefWidth(200);

        nom = new Label("nom");
        categorie = new Label("categorie");
        desc_label = new Label("description");
        desc_label.setWrapText(true);

        coffee_label.setSpacing(10);
        coffee_label.setPadding(new Insets(10));
        coffee_label.getChildren().add(nom);
        coffee_label.getChildren().add(categorie);
        coffee_label.getChildren().add(desc_label);


        return coffee_label;
    }

    private VBox CoffeelabelBoxInitialisation(VBox labelBox, ArrayList<Cafe> listeCafe)
    {
        labelBox.setSpacing(10);
        labelBox.setPadding(new Insets(10));
        
        for (int i = 0; i < listeCafe.size(); i++) 
        {
            Hyperlink link = new Hyperlink(listeCafe.get(i).get_nom());
            String nom_cafe = listeCafe.get(i).get_nom();
            String categ_cafe = listeCafe.get(i).get_categorie();
            String desc_cafe = listeCafe.get(i).get_description();
            link.setOnAction(event -> {
                nom.setText(nom_cafe);
                categorie.setText(categ_cafe);
                desc_label.setText(desc_cafe);
            });
            labelBox.getChildren().add(link);
        }

        return labelBox;
    }


}