package org.openjfx;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * classe représentant la fenêtre principal
 */
public class App extends Application
{


    public static void main(String[] args)
    {
        launch(args);
    }

   @Override
   /**
    * Méthode start de la classe principal
    */
    public void start(Stage primary_stage) throws Exception
    {
        ViewCreation view_creator = new ViewCreation();
        primary_stage = view_creator.screenCreation(primary_stage);
        primary_stage.show();

    }
}