package org.openjfx;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileAdministrator
{
    public String language_setting()
    {
        Scanner sc = new Scanner(System.in);

        String lang="";

        do {
            System.out.println("Please insert you're favorite language: \n-French:Fr \n-English:En");
            lang=sc.nextLine();
            if(!lang.equals("Fr")&&!lang.equals("En"))
                System.out.println("Please insert a correct language Fr for French, En for English");
        } while (!lang.equals("Fr") && !lang.equals("En"));
        
        sc.close();

        return lang;    
    }

    ArrayList<Cafe> lectureCafe(ArrayList<Cafe> param,String lang) throws FileNotFoundException
    {
        Scanner scan=null;
        try {
            File bdd=null;

            switch (lang)
            {
                case "Fr":
                case "En":
                    bdd = new File("src/main/resources/doc" + lang + ".txt");
                    break;
            
                default:
                    break;
            }
            
            scan = new Scanner(bdd);

            while(scan.hasNextLine())
            {
                String line = scan.nextLine();
                String[] cafe = line.split(";");
                param.add(new Cafe(Integer.valueOf(cafe[0]),cafe[1],cafe[2],cafe[3]));
            }
            scan.close();    
        } catch (Exception e) {
            System.out.println("Erreur lors de l'ouverture du fichier doc.txt");
            if(scan!=null)
                scan.close();
        }
        
        return param;
    }
}