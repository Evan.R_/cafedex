package org.openjfx;

/**
 * Classe public représentant un café
 */
public class Cafe{
    /**
     * Variable d'instance privé représentant le numéro du café
     */
    private int numero;
    
    /**
     * Variable d'instance privé représentant le nom du café
     */
    private String nom;
    
    /**
     * Variable d'instance privé représentant la catégorie du café
     */
    private String categorie;
   
    /**
     * Variable d'instance privé représentant la description
     */
    private String description;

    /**
     * Constructeur de la classe cafe
     * @param num le numero du café
     * @param nm le nom du café
     * @param categ la catégorie du café
     * @param descr la description du café
     */
    Cafe(int num,String nm,String categ,String descr)
    {
        this.numero=num;
        this.nom=nm;
        this.categorie=categ;
        this.description=descr;
    }

    /**
     * Getter du numero du café
     * @return le numero du café en entier
     */
    public int get_numero()
    {
        return this.numero;
    }

    /**
     * Getter du nom du café
     * @return le nom du cafe en String
     */
    public String get_nom()
    {
        return this.nom;
    }

    /**
     * Getter du t du café
     * @return la catégorie du pokemon en String
     */
    public String get_categorie()
    {
        return this.categorie;
    }

    /**
     * Getter de la description du café
     * @return la description du café en String
     */
    public String get_description()
    {
        return this.description;
    }
}
