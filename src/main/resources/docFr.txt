1;Espresso;Classique;L'espresso (et non pas expresso !) est un type de café célèbre d'Italie. Le produit final est un café épais avec une mousse crémeuse sur le dessus. En raison de son épaisseur et de son taux élevé de caféine par unité, il sert souvent de base pour d'autres boissons.
2;Ristretto;Classique;Il s'agit d'un Espresso préparé avec la même quantité de café mais la moitié de la quantité d'eau.
3;Lungo;Classique;Le Lungo, ou café allongé, est l'opposé au Ristretto. Cette boisson est faite avec plus d'eau.
4;Caffe Gommosa;Variante Espresso;Un expresso versé sur une seule guimauve.
5;Café Con Hielo;Variante Espresso;La version café glacé de l'espresso avec 50 ml d'espresso servi sur de la glace.
6;Doppio;Variante Espresso;Un double espresso
7;Espresso con panna;Variante Espresso;Café à la crème fouettée.
8;Espresso Romano;Variante Espresso;Un coup d'espresso servi avec une tranche de citron sur le côté.
9;Guillermo;Variante Espresso;Deux coups d'espresso chaud versés sur des tranches de citron vert, parfois servis sur de la glace.
10;Café Zorro;Variante Espresso;Un double espresso ajouté à de l'eau en utilisant un rapport 1: 1.
11;Café au lait;Café au lait;Café fort fait avec du lait échaudé dans un rapport 1: 1.
12;Latte;Café au lait;Espresso fait avec du lait cuit à la vapeur dans un rapport 1: 3 à 1: 5 avec un peu de mousse.
13;Cortado;Café au lait;Un espresso coupé avec une petite quantité de lait chaud pour réduire l'acidité.
14;Cappuccino;Café au lait;Espresso fait avec du lait chaud et de la mousse de lait cuit à la vapeur.
15;Noisette;Café au lait;Un espresso avec une goutte de lait. Cette faible quantité de lait va donner une couleur noisette au café.
16;Espressino;Café au lait;Une boisson à base d'espresso, de lait cuit à la vapeur et de cacao en poudre.
17;Macchiato;Café au lait;Un espresso fait avec une petite quantité de lait moussé, semblable à un cappuccino seulement plus fort
18;Café Viennois;Café au lait;Café ou expresso à base de crème fouettée avec du lait ajouté à certaines occasions.
19;Antoccino;Café au lait;Un seul coup d'espresso servi avec du lait cuit à la vapeur dans un rapport 1: 1.
20;Breve;Café au lait;Espresso servi avec moitié lait et moitié crème.
21;French Press Coffee;Café Infusé à froid;Café préparé avec un infuseur à presse française qui utilise un piston pour presser le café pour séparer la boisson finie du marc de café.
22;Café glacé;Café Infusé à froid;Légèrement différent du café infusé à froid, le café glacé est préparé à l'aide d'une méthode d'infusion à chaud, puis refroidi avant de servir.
23;Moka;Café Infusé à froid;Café infusé dans un pot de moka, qui fait passer l'eau à travers le café à l'aide de vapeur sous pression.
24;Café percolé;Café Infusé à froid;Une méthode d'infusion où l'eau chaude est recyclée à travers le marc de café en utilisant la gravité pour atteindre la force désirée.
25;Irish Coffee;Caffé bonus;Café combiné avec du whisky et de la crème et parfois avec du sucre.
26;Americano;Caffé bonus;Un espresso dans lequel de l'eau chaude, créant un café de même intensité mais de goût différent du café goutte à goutte ordinaire.